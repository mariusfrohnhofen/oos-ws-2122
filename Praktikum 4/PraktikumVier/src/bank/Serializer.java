package bank;

import java.lang.reflect.*;
import com.google.gson.*;

public class Serializer implements JsonSerializer<Transaction> {

	@Override
	public JsonElement serialize(Transaction transaction, Type type, JsonSerializationContext context) {
		JsonObject tmp = new JsonObject();
		
		Gson gson = new Gson().newBuilder().create();
		
		tmp.addProperty("CLASSNAME", transaction.getClass().getSimpleName());
		tmp.addProperty("INSTANCE", gson.toJson(transaction));
		
		return tmp;
	}

}
