package bank.exceptions;

/**
 * Wird ausgeführt wenn eine Transaktion bereits existiert
 */
public class TransactionAlreadyExistException extends Exception {
	
	public TransactionAlreadyExistException(String errorMessage) {
		super(errorMessage);
	}
}
