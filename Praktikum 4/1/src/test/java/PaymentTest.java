import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import bank.*;

public class PaymentTest {
	
	Payment p1;
	Payment p2;
	Payment p3;
	Payment p4;
	
	@BeforeEach
	void init() {
		p1 = new Payment("Test Date", 1000, "Test Description");
		p2 = new Payment("Test Date 2", 2000, "Test Description 2", 0.01, 0.05);
		p3 = new Payment(p1);
		p4 = new Payment("Test Date 4", -500, "Test Description 4", 0.01, 0.05);
	}
	
	@Test
	void KontruktorTest() {
		assertEquals(1000, p1.getAmount());
		assertEquals(2000, p2.getAmount());
	}
	
	@Test
	void CopyKonstruktorTest() {
		assertEquals(p1.getDate(), p3.getDate());
		assertEquals(p1.getAmount(), p3.getAmount());
		assertEquals(p1.getDescription(), p3.getDescription());
		assertEquals(p1.getIncomingInterest(), p3.getIncomingInterest());
		assertEquals(p1.getOutgoingInterest(), p3.getOutgoingInterest());
	}
	
	@Test
	void calculateTest() {
		assertEquals(1000, p1.calculate());
		assertEquals(1980, p2.calculate());
		assertEquals(1000, p3.calculate());
		assertEquals(-525, p4.calculate());
	}
	
	@Test
	void equalsTest() {
		assertEquals(false, p1.equals(p2));
		assertEquals(true, p1.equals(p3));
		assertEquals(false, p2.equals(p3));
	}
	
	@Test
	void toStringTest() {
		assertEquals("Date: Test Date, Amount: 1000.0, Description: Test Description, Incoming Interest: 0.0, Outgoing Interest: 0.0", p1.toString());
		assertEquals("Date: Test Date 2, Amount: 1980.0, Description: Test Description 2, Incoming Interest: 0.01, Outgoing Interest: 0.05", p2.toString());
		assertEquals("Date: Test Date, Amount: 1000.0, Description: Test Description, Incoming Interest: 0.0, Outgoing Interest: 0.0", p3.toString());
		assertEquals("Date: Test Date 4, Amount: -525.0, Description: Test Description 4, Incoming Interest: 0.01, Outgoing Interest: 0.05", p4.toString());
	}
}
