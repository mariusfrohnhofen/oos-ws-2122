import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import bank.*;

public class TransferTest {
	
	Transfer t1;
	Transfer t2;
	Transfer t3;
	Transfer t4;
	
	@BeforeEach
	void init() {
		t1 = new IncomingTransfer("Test Date", 500, "Test Description", "Markus", "Marius");
		t2 = new IncomingTransfer("Test Date 2", 1000, "Test Description 2", "Andrea", "Marius");
		t3 = new OutgoingTransfer("Test Date 3", 1500, "Test Description 3", "Marius", "Fritz");
		t4 = new OutgoingTransfer(t3);
	}
	
	@Test
	void KontruktorTest() {
		assertEquals(500, t1.getAmount());
		assertEquals(1000, t2.getAmount());
		assertEquals(1500, t3.getAmount());
		assertEquals(1500, t4.getAmount());
	}
	
	@Test
	void CopyKonstruktorTest() {
		assertEquals(t3.getDate(), t4.getDate());
		assertEquals(t3.getAmount(), t4.getAmount());
		assertEquals(t3.getDescription(), t4.getDescription());
		assertEquals(t3.getSender(), t4.getSender());
		assertEquals(t3.getRecipient(), t4.getRecipient());
	}
	
	@Test
	void calculateTest() {
		assertEquals(500, t1.calculate());
		assertEquals(1000, t2.calculate());
		assertEquals(-1500, t3.calculate());
		assertEquals(-1500, t4.calculate());
	}
	
	@Test
	void equalsTest() {
		assertEquals(false, t1.equals(t2));
		assertEquals(true, t3.equals(t4));
		assertEquals(false, t2.equals(t4));
	}
	
	@Test
	void toStringTest() {
		assertEquals("Date: Test Date, Amount: 500.0, Description: Test Description, Sender: Markus, Recipient: Marius", t1.toString());
		assertEquals("Date: Test Date 2, Amount: 1000.0, Description: Test Description 2, Sender: Andrea, Recipient: Marius", t2.toString());
		assertEquals("Date: Test Date 3, Amount: -1500.0, Description: Test Description 3, Sender: Marius, Recipient: Fritz", t3.toString());
		assertEquals("Date: Test Date 3, Amount: -1500.0, Description: Test Description 3, Sender: Marius, Recipient: Fritz", t4.toString());
	}
}
