
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import org.junit.jupiter.api.*;

import bank.*;
import bank.exceptions.AccountAlreadyExistsException;
import bank.exceptions.AccountDoesNotExistException;
import bank.exceptions.TransactionAlreadyExistException;
import bank.exceptions.TransactionDoesNotExistException;


public class PrivateBankTest {
	
	PrivateBank pb;
	PrivateBank pb2;
	PrivateBank pb3;
	Payment p;
	
	@BeforeEach
	void init() throws IOException, AccountAlreadyExistsException {
		pb = new PrivateBank("Private Bank", 0.05, 0.1, "C:\\Users\\Mariuss\\Private Bank 1");
		pb2 = new PrivateBank("Private Bank 2", 0.5, 0.01, "C:\\Users\\Mariuss\\Private Bank 2");
		pb3 = new PrivateBank(pb);
		p = new Payment("Test", 100, "Test");
	}
	
	@Test
	void konstruktorTest() {
		assertEquals("Private Bank", pb.getName());
		assertEquals(0.05, pb.getIncomingInterest());
		assertEquals(0.1, pb.getOutgoingInterest());
	}
	
	@Test
	void readAccountsTest() throws IOException, AccountAlreadyExistsException {
		pb.accountToTransactions.clear();
		pb.readAccounts();
		assertEquals("Marius", ((IncomingTransfer)pb.accountToTransactions.get("Marius").get(1)).getRecipient());
	}
	
	@Test
	void writeAccountTest() throws IOException {
		pb.writeAccount("Marius");
	}
	
	@Test
	void createAccountTest() throws AccountAlreadyExistsException, IOException {
		pb.accountToTransactions.clear();
		pb.createAccount("Test");
	}
	
	@Test
	void addTransactionAndContainsTransactionTest() throws TransactionAlreadyExistException, AccountDoesNotExistException, IOException, AccountAlreadyExistsException {
		Payment p2 = new Payment("Test2", 100, "Test2");
		pb.addTransaction("Test", p);
		assertEquals(false, pb.containsTransaction("Test", p));
		assertEquals(false, pb.containsTransaction("Test", p2));
	}
	
	@Test
	void removeTransactionTest() throws TransactionDoesNotExistException, AccountDoesNotExistException, IOException, AccountAlreadyExistsException, TransactionAlreadyExistException {
		if (!pb.containsTransaction("Test", p)) {
			pb.addTransaction("Test", p);
		}
		pb.removeTransaction("Test", p);
		assertEquals(false, pb.containsTransaction("Test", p));
	}
	
	@Test
	void getAccountBalanceTest() throws IOException, AccountAlreadyExistsException {
		assertEquals(800, pb.getAccountBalance("Marius"));
	}
	
	@Test
	void getTransactionsByTypeTest() throws IOException, AccountAlreadyExistsException {
		assertEquals(1000, pb.getTransactionsByType("Marius", true).get(0).getAmount());
		assertEquals(300, pb.getTransactionsByType("Marius", false).get(0).getAmount());
	}
	
	@Test
	void getTransactionsSortedTest() throws IOException, AccountAlreadyExistsException {
		assertEquals(300, pb.getTransactionsSorted("Marius", true).get(0).getAmount());
		assertEquals(1000, pb.getTransactionsSorted("Marius", false).get(0).getAmount());
	}
	
	@Test
	void toStringTest() {
		assertEquals("Name: Private Bank, Incoming Interest: 0.05, Outgoing Interest: 0.1", pb.toString());
		assertEquals("Name: Private Bank 2, Incoming Interest: 0.5, Outgoing Interest: 0.01", pb2.toString());
		assertEquals("Name: Private Bank, Incoming Interest: 0.05, Outgoing Interest: 0.1", pb3.toString());
	}
	
	@Test
	void equalsTest() {
		assertEquals(false, pb.equals(pb2));
		assertEquals(false, pb2.equals(pb3));
	}
	
	@Test
	void exceptionTest() {
		AccountAlreadyExistsException thrown = Assertions.assertThrows(AccountAlreadyExistsException.class, () -> {
			pb.createAccount("Marius");
		});
		
		Assertions.assertEquals("Account Marius existiert bereits", thrown.getMessage());
		
		AccountDoesNotExistException thrown2 = Assertions.assertThrows(AccountDoesNotExistException.class, () -> {
			pb.addTransaction("Marc", p);
		});
		
		Assertions.assertEquals("Account Marc existiert nicht", thrown2.getMessage());
		
		TransactionDoesNotExistException thrown4 = Assertions.assertThrows(TransactionDoesNotExistException.class, () -> {
			Payment tmp = new Payment("Test Date", 100000, "Test Description");
			pb.removeTransaction("Marius", tmp);
		});
		
		Assertions.assertEquals("Transaction Test Description existiert nicht in Account Marius", thrown4.getMessage());
	}
}
