/**
 * Ein Bank-Programm zum Praktikum OOS WS 2021/22
 * Praktikum 2
 *
 * @author Marius Frohnhofen
 */

import java.io.IOException;
import java.util.List;

import bank.*;
import bank.exceptions.*;
import com.google.gson.*;
import java.lang.reflect.*;

/**
 * Klasse Main, hier werden Tests zur Implementation durchgeführt
 */
public class Main {
	public static void main(String[] args) throws AccountAlreadyExistsException, TransactionAlreadyExistException, AccountDoesNotExistException, TransactionDoesNotExistException, IOException {
		
		PrivateBank pb1 = new PrivateBank("Private Bank 1", 0.05, 0.1, "C:\\Users\\Mariuss\\Private Bank 1");
		/**
		PrivateBank pb2 = new PrivateBank("Private Bank 2", 0.1, 0.05, "");
		
		PrivateBankAlt pba1 = new PrivateBankAlt("Private Bank Alt 1", 0.05, 0.1);
		PrivateBankAlt pba2 = new PrivateBankAlt("Private Bank Alt 2", 0.1, 0.05);
		
		// Testen der Funktionalität der implementierten Funktionen des Interfaces Bank
		
		//Getter und Setter
		System.out.println("Setter und Getter:\n");
		System.out.println(pb1.getName() + ", " + pb1.getIncomingInterest() + ", " + pb1.getOutgoingInterest());
		System.out.println(pb2.getName() + ", " + pb2.getIncomingInterest() + ", " + pb2.getOutgoingInterest());
		System.out.println(pba1.getName() + ", " + pba1.getIncomingInterest() + ", " + pba1.getOutgoingInterest());
		System.out.println(pba2.getName() + ", " + pba2.getIncomingInterest() + ", " + pba2.getOutgoingInterest());
		
		pb1.setName("New Private Bank 1");
		pb1.setIncomingInterest(0.1);
		pb1.setOutgoingInterest(0.05);
		
		pb2.setName("New Private Bank 2");
		pb2.setIncomingInterest(0.05);
		pb2.setOutgoingInterest(0.1);
		
		pba1.setName("New Private Bank Alt 1");
		pba1.setIncomingInterest(0.1);
		pba1.setOutgoingInterest(0.05);
		
		pba2.setName("New Private Bank Alt 2");
		pba2.setIncomingInterest(0.05);
		pba2.setOutgoingInterest(0.1);
		
		System.out.println();
		
		System.out.println(pb1.getName() + ", " + pb1.getIncomingInterest() + ", " + pb1.getOutgoingInterest());
		System.out.println(pb2.getName() + ", " + pb2.getIncomingInterest() + ", " + pb2.getOutgoingInterest());
		System.out.println(pba1.getName() + ", " + pba1.getIncomingInterest() + ", " + pba1.getOutgoingInterest());
		System.out.println(pba2.getName() + ", " + pba2.getIncomingInterest() + ", " + pba2.getOutgoingInterest());
		
		System.out.println("\n-----");
		
		
		
		// Create Account
		System.out.println("\nCreate Account:\n");
		
		pb1.createAccount("Marius");
		
		System.out.println(pb1.accountToTransactions);
		
		System.out.println("\n-----");
		
		
		
		// Add Transaction
		System.out.println("\nAdd Transaction:\n");
		
		Payment p = new Payment("Test Date", 1000, "Test Description");
		
		pb1.addTransaction("Marius", p);
		
		System.out.println(pb1.accountToTransactions);
		
		System.out.println("\n-----");
		
		
		
		// Remove Transaction
		System.out.println("\nRemove Transaction:\n");
		
		pb1.removeTransaction("Marius", p);
		
		System.out.println(pb1.accountToTransactions);
		
		System.out.println("\n-----");
		
		
		
		// Contains Transaction
		System.out.println("\nContains Transaction:\n");
		
		System.out.println(pb1.containsTransaction("Marius", p));
		pb1.addTransaction("Marius", p);
		System.out.println(pb1.containsTransaction("Marius", p));
		
		System.out.println("\n-----");
		
		
		
		// Get Account Balance
		System.out.println("\nGet Account Balance:\n");
		
		IncomingTransfer it = new IncomingTransfer("Test Date", 150, "Test Description", "Annette", "Marius");
		OutgoingTransfer ot = new OutgoingTransfer("Test Date", 300, "Test Description", "Marius", "Markus");
		
		System.out.println(pb1.getAccountBalance("Marius"));
		pb1.addTransaction("Marius", it);
		System.out.println(pb1.getAccountBalance("Marius"));
		pb1.addTransaction("Marius", ot);
		System.out.println(pb1.getAccountBalance("Marius"));
		
		System.out.println("\n-----");
		
		
		
		// Get Transactions
		System.out.println("\nGet Transactions:\n");
		
		List<Transaction> tmp = pb1.getTransactions("Marius");
		
		for (int i = 0; i < tmp.size(); i++) {
			System.out.println(tmp.get(i));
		}
		
		System.out.println("\n-----");
		
		
		
		// Get Transactions Sorted
		System.out.println("\nGet Transactions Sorted:\n");
		
		tmp = pb1.getTransactionsSorted("Marius", true);
		
		System.out.println("Aufsteigend");
		for (int i = 0; i < tmp.size(); i++) {
			System.out.println(tmp.get(i));
		}
		
		tmp = pb1.getTransactionsSorted("Marius", false);
		
		System.out.println("\nAbsteigend");
		for (int i = 0; i < tmp.size(); i++) {
			System.out.println(tmp.get(i));
		}
		
		System.out.println("\n-----");
		
		
		
		// Get Transactions By Type
		System.out.println("\nGet Transactions By Type:\n");
		
		tmp = pb1.getTransactionsByType("Marius", true);
		
		System.out.println("Positiv");
		for (int i = 0; i < tmp.size(); i++) {
			System.out.println(tmp.get(i));
		}
		
		tmp = pb1.getTransactionsByType("Marius", false);
		
		System.out.println("\nNegativ");
		for (int i = 0; i < tmp.size(); i++) {
			System.out.println(tmp.get(i));
		}
		
		System.out.println("\n-----");
		
		
		
		// Testen der Funktion PrivateBank#equals()
		System.out.println("\nequals()-Funktion:\n");
		
		PrivateBank pb3 = new PrivateBank("Test Bank", 0.05, 0.1, "");
		PrivateBank pb4 = new PrivateBank("Test Bank", 0.05, 0.1, "");
		
		System.out.println(pb3.equals(pb4));
		
		pb4.createAccount("Marius");
		System.out.println(pb3.equals(pb4));
		
		pb3.createAccount("Marius");
		System.out.println(pb3.equals(pb4));
		
		pb4.addTransaction("Marius", p);
		System.out.println(pb3.equals(pb4));
		
		pb3.addTransaction("Marius", p);
		System.out.println(pb3.equals(pb4));
		
		pb1.writeAccount("Marius");
		
		**/
		pb1.readAccounts();
		System.out.println(pb1.accountToTransactions);
		
	}
}