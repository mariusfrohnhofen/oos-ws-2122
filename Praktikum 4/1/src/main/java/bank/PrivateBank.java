package bank;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gson.*;

import bank.exceptions.*;

/**
 * Klasse PrivateBank zur Darstellung einer Bank mit Kunden und deren Transaktionen
 */
public class PrivateBank implements Bank {
	
	/**
	 * Pfad zur Directory mit allen Dateien zu den Konten
	 */
	private String directoryPath;
	
	/**
	 * Name der Bank
	 */
	private String name;
	
	/**
	 * Zinsen, die bei einer Einzahlung anfallen
	 * 
	 * Wert zwischen 0 und 1
	 */
	private double incomingInterest;
	
	/**
	 * Zinsen, die bei einer Auszahlung anfallen
	 * 
	 * Wert zwischen 0 und 1
	 */
	private double outgoingInterest;
	
	/**
	 * Hashmap, welche einen String mit dem Namen des Kunden zu dessen Liste von Transaktionen verkn�pft
	 */
	public Map<String, List<Transaction>> accountToTransactions = new HashMap<String, List<Transaction>>();
	
	/**
	 * Konstruktor, welcher name, incomingInterest und outgoingInterest setzt
	 * 
	 * @param name
	 * @param incomingInterest
	 * @param outgoingInterest
	 * @param directoryPath
	 * @throws IOException 
	 * @throws AccountAlreadyExistsException 
	 */
	public PrivateBank(String name, double incomingInterest, double outgoingInterest, String directoryPath) throws IOException, AccountAlreadyExistsException {
		this.name = name;
		this.incomingInterest = incomingInterest;
		this.outgoingInterest = outgoingInterest;
		this.directoryPath = directoryPath;
		
		readAccounts();
	}
	
	/**
	 * Copy-Konstruktor
	 * 
	 * @param pb
	 * @throws IOException 
	 * @throws AccountAlreadyExistsException 
	 */
	public PrivateBank(PrivateBank pb) throws IOException, AccountAlreadyExistsException {
		this.name = pb.name;
		this.incomingInterest = pb.incomingInterest;
		this.outgoingInterest = pb.outgoingInterest;
		this.directoryPath = pb.directoryPath;
		
		readAccounts();
	}
	
	/**
	 * Accounts werden aus Datei ausgelesen
	 * @throws AccountAlreadyExistsException 
	 */
	public void readAccounts() throws IOException, AccountAlreadyExistsException {
		accountToTransactions.clear();
		GsonBuilder gsonBuilder = new GsonBuilder();
		JsonDeserializer<Transaction> deserializer = new Deserializer();
		
		gsonBuilder.registerTypeAdapter(Transaction.class, deserializer);
		
		Gson customGson = gsonBuilder.setPrettyPrinting().create();
		
		JsonParser parser = new JsonParser();
		
		File directory = new File(directoryPath);
		File filesList[] = directory.listFiles();
		
		for(File file : filesList) {
			
			JsonArray a = (JsonArray) parser.parse(new FileReader(directoryPath + "/" + file.getName()));
		  
			String accountName = file.getName().replace(".json", "");
			  
			List<Transaction> tmpList = new ArrayList<>();
			
			for (JsonElement e : a) {
				tmpList.add(customGson.fromJson(e, Transaction.class));
			}
			  
			createAccount(accountName, tmpList);
		}
	}
	
	/**
	 * Account wird in Datei abgelegt
	 * @param name
	 */
	public void writeAccount(String name) throws IOException {
		GsonBuilder gsonBuilder = new GsonBuilder();
		JsonSerializer<Transaction> serializer = new Serializer();
		
		FileWriter w = new FileWriter(directoryPath + "/" + name + ".json", false);
		
		gsonBuilder.registerTypeAdapter(Payment.class, serializer);
		gsonBuilder.registerTypeAdapter(IncomingTransfer.class, serializer);
		gsonBuilder.registerTypeAdapter(OutgoingTransfer.class, serializer);
		
		Gson customGson = gsonBuilder.setPrettyPrinting().create();
		
		customGson.toJson(accountToTransactions.get(name), w);
		
		w.close();
	}
	
	/**
	 * Get-Methode f�r Name
	 * 
	 * @return name-Attribut
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Set-Methode f�r Name
	 * 
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Get-Methode f�r Incoming Interest
	 * 
	 * @return incomingInterest-Attribut
	 */
	public double getIncomingInterest() {
		return incomingInterest;
	}
	
	/**
	 * Set-Methode f�r Incoming Interest
	 * 
	 * @param incomingInterest
	 */
	public void setIncomingInterest(double incomingInterest) {
		this.incomingInterest = incomingInterest;
	}
	
	/**
	 * Get-Methode f�r Outgoing Interest
	 * 
	 * @return outgoingInterest-Attribut
	 */
	public double getOutgoingInterest() {
		return outgoingInterest;
	}
	
	/**
	 * Set-Methode f�r Outgoing Interest
	 * 
	 * @param outgoingInterest
	 */
	public void setOutgoingInterest(double outgoingInterest) {
		this.outgoingInterest = outgoingInterest;
	}
	
	/**
     * Adds an account to the bank. If the account already exists, an exception is thrown.
     *
     * @param account the account to be added
     * @throws AccountAlreadyExistsException if the account already exists
	 * @throws IOException 
     */
	@Override
	public void createAccount(String account) throws AccountAlreadyExistsException, IOException {
		
		if (accountToTransactions.containsKey(account)) {
			throw new AccountAlreadyExistsException("Account " + account + " existiert bereits");
		}
		else {
			accountToTransactions.put(account, new ArrayList<Transaction>());
		}
		
		writeAccount(account);
	}
	
	/**
     * Adds an account (with all specified transactions) to the bank. If the account already exists,
     * an exception is thrown.
     *
     * @param account the account to be added
     * @throws AccountAlreadyExistsException if the account already exists
	 * @throws IOException 
     */
	@Override
	public void createAccount(String account, List<Transaction> transactions) throws AccountAlreadyExistsException, IOException {
		
		if (accountToTransactions.containsKey(account)) {
			throw new AccountAlreadyExistsException("Account " + account + " existiert bereits");
		}
		else {
			accountToTransactions.put(account, transactions);
		}
		
		writeAccount(account);
	}
	
	/**
     * Adds a transaction to an account. If the specified account does not exist, an exception is
     * thrown. If the transaction already exists, an exception is thrown.
     *
     * @param account     the account to which the transaction is added
     * @param transaction the transaction which is added to the account
     * @throws TransactionAlreadyExistException if the transaction already exists
	 * @throws IOException 
	 * @throws AccountAlreadyExistsException 
     */
	@Override
	public void addTransaction(String account, Transaction transaction)
			throws TransactionAlreadyExistException, AccountDoesNotExistException, IOException, AccountAlreadyExistsException {
		readAccounts();
		
		if (accountToTransactions.containsKey(account)) {
			if (transaction instanceof Payment) {
				((Payment)transaction).setIncomingInterest(this.incomingInterest);
				((Payment)transaction).setOutgoingInterest(this.outgoingInterest);
			}
			accountToTransactions.get(account).add(transaction);
		}
		else {
			throw new AccountDoesNotExistException("Account " + account + " existiert nicht");
		}
		
		writeAccount(account);
	}
	
	/**
     * Removes a transaction from an account. If the transaction does not exist, an exception is
     * thrown.
     *
     * @param account     the account from which the transaction is removed
     * @param transaction the transaction which is added to the account
     * @throws TransactionDoesNotExistException if the transaction cannot be found
	 * @throws IOException 
	 * @throws AccountAlreadyExistsException 
     */
	@Override
	public void removeTransaction(String account, Transaction transaction) throws TransactionDoesNotExistException, AccountDoesNotExistException, IOException, AccountAlreadyExistsException {
		if (accountToTransactions.containsKey(account)) {
			if (accountToTransactions.get(account).contains(transaction)) {
				accountToTransactions.get(account).remove(transaction);
			}
			else {
				throw new TransactionDoesNotExistException("Transaction " + transaction.getDescription() + " existiert nicht in Account " + account);
			}
		}
		else {
			throw new AccountDoesNotExistException("Account " + account + " existiert nicht");
		}
		
		writeAccount(account);
	}
	
	/**
     * Checks whether the specified transaction for a given account exists.
     *
     * @param account     the account from which the transaction is checked
     * @param transaction the transaction which is added to the account
	 * @throws IOException 
	 * @throws AccountAlreadyExistsException 
     */
	@Override
	public boolean containsTransaction(String account, Transaction transaction) throws IOException, AccountAlreadyExistsException {
		readAccounts();
		
		if (accountToTransactions.get(account).contains(transaction)) {
			return true;
		}
		return false;
	}
	
	/**
     * Calculates and returns the current account balance.
     *
     * @param account the selected account
     * @return the current account balance
	 * @throws IOException 
	 * @throws AccountAlreadyExistsException 
     */
	@Override
	public double getAccountBalance(String account) throws IOException, AccountAlreadyExistsException {
		readAccounts();
		
		double bal = 0;
		
		for (int i = 0; i < accountToTransactions.get(account).size(); i++) {
			bal = bal + accountToTransactions.get(account).get(i).calculate();
		}
		
		return bal;
	}
	
	/**
     * Returns a list of transactions for an account.
     *
     * @param account the selected account
     * @return the list of transactions
	 * @throws IOException 
	 * @throws AccountAlreadyExistsException 
     */
	@Override
	public List<Transaction> getTransactions(String account) throws IOException, AccountAlreadyExistsException {
		readAccounts();
		
		return accountToTransactions.get(account);
	}
	
	/**
     * Returns a sorted list (-> calculated amounts) of transactions for a specific account . Sorts the list either in ascending or descending order
     * (or empty).
     *
     * @param account the selected account
     * @param asc     selects if the transaction list is sorted ascending or descending
     * @return the list of transactions
	 * @throws IOException 
	 * @throws AccountAlreadyExistsException 
     */
	@Override
	public List<Transaction> getTransactionsSorted(String account, boolean asc) throws IOException, AccountAlreadyExistsException {
		readAccounts();
		
		List<Transaction> result = accountToTransactions.get(account);
		
		if (asc) {
			result.sort(Comparator.comparing(Transaction::calculate));
		}
		else {
			result.sort(Comparator.comparing(Transaction::calculate).reversed());
		}
		
		return result;
	}
	
	/**
     * Returns a list of either positive or negative transactions (-> calculated amounts).
     *
     * @param account  the selected account
     * @param positive selects if positive  or negative transactions are listed
     * @return the list of transactions
	 * @throws IOException 
	 * @throws AccountAlreadyExistsException 
     */
	@Override
	public List<Transaction> getTransactionsByType(String account, boolean positive) throws IOException, AccountAlreadyExistsException {
		readAccounts();
		
		List<Transaction> result = new ArrayList<>();
		
		for (int i = 0; i < accountToTransactions.get(account).size(); i++) {
			if (positive) {
				if (accountToTransactions.get(account).get(i).calculate() >= 0) {
					result.add(accountToTransactions.get(account).get(i));
				}
			}
			else {
				if (accountToTransactions.get(account).get(i).calculate() < 0) {
					result.add(accountToTransactions.get(account).get(i));
				}
			}
		}
		
		return result;
	}
	
	/**
	 * Override von Object.toString()
	 * 
	 * @return Transfer-Object als String
	 */
	@Override
	public String toString() {
		return "Name: " + name + ", Incoming Interest: " + incomingInterest + ", Outgoing Interest: " + outgoingInterest;
	}
	
	/**
	 * Override von Object.equals(Object o)
	 * 
	 * @return true or false
	 */
	@Override
	public boolean equals(Object o) {
		if (o instanceof PrivateBank) {
			if (name == ((PrivateBank) o).getName() && incomingInterest == ((PrivateBank) o).getIncomingInterest() && outgoingInterest == ((PrivateBank) o).getOutgoingInterest() && accountToTransactions.equals(((PrivateBank) o).accountToTransactions)) {
				return true;
			}
		}
		return false;
	}

}
