package bank;

import java.lang.reflect.*;
import com.google.gson.*;

public class Serializer implements JsonSerializer<Transaction> {

	@Override
	public JsonElement serialize(Transaction transaction, Type type, JsonSerializationContext context) {
		JsonObject tmp = new JsonObject();
		
		tmp.addProperty("CLASSNAME", transaction.getClass().getSimpleName());
		tmp.add("INSTANCE", new Gson().toJsonTree(transaction));
		
		return tmp;
	}

}
