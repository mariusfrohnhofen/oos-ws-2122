/**
 * Ein Bank-Programm zum Praktikum OOS WS 2021/22
 * Praktikum 2
 *
 * @author Marius Frohnhofen
 */

package bank;

/**
 * Interface CalculateBill
 * 
 * Diese Klasse wird in Transaction implementiert
 */
public interface CalculateBill {
	
	/**
	 * Methodenkopf der abstrakten Methode calculate
	 */
	public abstract double calculate();
}
