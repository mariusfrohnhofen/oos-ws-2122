package bank;

import java.lang.reflect.Type;

import com.google.gson.*;

public class Deserializer implements JsonDeserializer<Transaction> {

	@Override
	public Transaction deserialize(JsonElement element, Type type, JsonDeserializationContext context) {
		JsonObject json = element.getAsJsonObject();
	
		String cn = json.get("CLASSNAME").getAsString();
		
		json = (JsonObject) json.get("INSTANCE");
		
		if (cn.equals("Payment")) {
			return new Payment(
				json.get("date").getAsString(),
				json.get("amount").getAsDouble(),
				json.get("description").getAsString(),
				json.get("incomingInterest").getAsDouble(),
				json.get("outgoingInterest").getAsDouble()
			);
		}
		else if (cn.equals("IncomingTransfer")) {
			return new IncomingTransfer(
				json.get("date").getAsString(),
				json.get("amount").getAsDouble(),
				json.get("description").getAsString(),
				json.get("sender").getAsString(),
				json.get("recipient").getAsString()
			);
		}
		else if (cn.equals("OutgoingTransfer")) {
			return new OutgoingTransfer(
				json.get("date").getAsString(),
				json.get("amount").getAsDouble(),
				json.get("description").getAsString(),
				json.get("sender").getAsString(),
				json.get("recipient").getAsString()
			);
		}
		return null;
	}

}
