package bank;

/**
 * IncomingTransfer-Klasse, welche von Transfer erbt
 * 
 * Repräsentiert eine eingehende Zahlung
 */
public class IncomingTransfer extends Transfer {
	
	/**
	 * Copy-Konstruktor
	 * 
	 * @param t
	 */
	public IncomingTransfer(Transfer t) {
		super(t);
	}
	
	/**
	 * Konstruktor, welcher date, amount, description, sender und recipient setzt
	 * 
	 * @param date
	 * @param amount
	 * @param description
	 * @param sender
	 * @param recipient
	 */
	public IncomingTransfer(String date, double amount, String description, String sender, String recipient) {
		super(date, amount, description, sender, recipient);
	}
	
	/**
	 * Konstruktor, welcher date, amount und description setzt
	 * 
	 * @param date
	 * @param amount
	 * @param description
	 */
	public IncomingTransfer(String date, double amount, String description) {
		super(date, amount, description);
	}
	
	/**
	 * Berechnet die Kosten einer Überweisung
	 * 
	 * @return amount
	 */
	@Override
	public double calculate() {
		return amount;
	}
}
