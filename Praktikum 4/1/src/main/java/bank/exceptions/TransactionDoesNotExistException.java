package bank.exceptions;

/**
 * Wird ausgeführt wenn eine Transaktion nicht existiert
 */
public class TransactionDoesNotExistException extends Exception {
	
	public TransactionDoesNotExistException(String errorMessage) {
		super(errorMessage);
	}
}
