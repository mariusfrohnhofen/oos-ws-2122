package bank.exceptions;

/**
 * Wird ausgeführt wenn ein Account bereits existiert
 */
public class AccountAlreadyExistsException extends Exception {
	
	public AccountAlreadyExistsException(String errorMessage) {
		super(errorMessage);
	}
}
