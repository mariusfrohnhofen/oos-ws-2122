package bank.exceptions;

/**
 * Wird ausgeführt wenn ein Account nicht existiert
 */
public class AccountDoesNotExistException extends Exception {
	
	public AccountDoesNotExistException(String errorMessage) {
		super(errorMessage);
	}
}
