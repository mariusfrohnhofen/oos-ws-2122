/**
 * Ein Bank-Programm zum Praktikum OOS WS 2021/22
 * Praktikum 2
 *
 * @author Marius Frohnhofen
 * @see Payment
 */

package bank;

/**
 * Payment-Klasse, welche von Transaction erbt
 * 
 * Diese Klasse soll Ein- und Auszahlungen repraesentieren
 */
public class Payment extends Transaction {

	/**
	 * Zinsen, die bei einer Einzahlung anfallen
	 * 
	 * Wert zwischen 0 und 1
	 */
	private double incomingInterest;
	
	/**
	 * Zinsen, die bei einer Auszahlung anfallen
	 * 
	 * Wert zwischen 0 und 1
	 */
	private double outgoingInterest;

	/**
	 * Get-Methode f�r Incoming Interest
	 * 
	 * @return incomingInterest-Attribut
	 */
	public double getIncomingInterest() {
		return incomingInterest;
	}

	/**
	 * Set-Methode f�r incomingInterest
	 * 
	 * @param incomingInterest
	 */
	public void setIncomingInterest(double incomingInterest) {
		this.incomingInterest = incomingInterest;
	}

	/**
	 * Get-Methode f�r Outgoing Interest
	 * 
	 * @return outgoingInterest-Attribut
	 */
	public double getOutgoingInterest() {
		return outgoingInterest;
	}

	/**
	 * Set-Methode f�r outgoingInterest
	 * 
	 * @param outgoingInterest
	 */
	public void setOutgoingInterest(double outgoingInterest) {
		this.outgoingInterest = outgoingInterest;
	}
	
	/**
	 * Konstruktor, welcher date, amount und description setzt
	 * 
	 * @param date
	 * @param amount
	 * @param description
	 */
	public Payment(String date, double amount, String description) {
		super(date, amount, description);
	}

	/**
	 * Konstruktor, welcher date, amount, description, incomingInterest und outgoingInterest setzt
	 * 
	 * @param date
	 * @param amount
	 * @param description
	 * @param incomingInterest
	 * @param outgoingInterest
	 */
	public Payment(String date, double amount, String description, double incomingInterest, double outgoingInterest) {
		super(date, amount, description);
		this.incomingInterest = incomingInterest;
		this.outgoingInterest = outgoingInterest;
	}

	/**
	 * Copy-Konstruktor
	 * 
	 * @param p
	 */
	public Payment(Payment p) {
		this.date = p.date;
		this.amount = p.amount;
		this.description = p.description;
		this.incomingInterest = p.incomingInterest;
		this.outgoingInterest = p.outgoingInterest;
	}

	/**
	 * Override von Object.toString()
	 * 
	 * @return Payment-Object als String
	 */
	@Override
	public String toString() {
		return super.toString() + ", Incoming Interest: " + incomingInterest + ", Outgoing Interest: "
				+ outgoingInterest;
	}

	/**
	 * Berechnet die Kosten einer Ein- oder Auszahlung
	 * 
	 * @return amount nach Berechnung der Kosten
	 */
	public double calculate() {
		if (amount > 0) {
			/**
			 * Einzahlung
			 */
			return amount - (amount * incomingInterest);
		} else {
			/**
			 * Auszahlung
			 */
			return amount + (amount * outgoingInterest);
		}
	}

	/**
	 * Override von Object.equals(Object o)
	 * 
	 * @return true or false
	 */
	@Override
	public boolean equals(Object o) {
		if (o instanceof Payment) {
			if (super.equals(o) && incomingInterest == ((Payment) o).getIncomingInterest()
					&& outgoingInterest == ((Payment) o).getOutgoingInterest()) {
				return true;
			}
		}
		return false;
	}
}