/**
 * Ein Bank-Programm zum Praktikum OOS WS 2021/22
 * Praktikum 2
 *
 * @author Marius Frohnhofen
 */

package bank;

/**
 * Transfer-Klasse, welche von Transaction erbt
 * 
 * Diese Klasse soll im Kontext von �berweisungen verwendet werden
 */
public class Transfer extends Transaction {
	
	/**
	 * Gibt an, welcher Akteur die Geldmenge, die in amount angegeben wurde, �berwiesen hat
	 */
	private String sender;
	
	/**
	 * Gibt an, welcher Akteur die Geldmenge, die in amount angegeben wurde, �berwiesen bekommen hat
	 */
	private String recipient;
	
	
	/**
	 * Get-Methode f�r Sender
	 * 
	 * @return sender-Attribut
	 */
	public String getSender() {
		return sender;
	}
	
	/**
	 * Set-Methode f�r Sender
	 * 
	 * @param sender
	 */
	public void setSender(String sender) {
		this.sender = sender;
	}
	
	/**
	 * Get-Methode f�r Recipient
	 * 
	 * @return recipient-Attribut
	 */
	public String getRecipient() {
		return recipient;
	}
	
	/**
	 * Set-Methode f�r Recipient
	 * 
	 * @param recipient
	 */
	public void setRecipient(String recipient) {
		this.recipient = recipient;
	}
	
	/**
	 * Konstruktor, welcher date, amount und description setzt
	 * 
	 * @param date
	 * @param amount
	 * @param description
	 */
	public Transfer(String date, double amount, String description) {
		super(date, amount, description);
	}
	
	/**
	 * Konstruktor, welcher date, amount, description, sender und recipient setzt
	 * 
	 * @param date
	 * @param amount
	 * @param description
	 * @param sender
	 * @param recipient
	 */
	public Transfer(String date, double amount, String description, String sender, String recipient) {
		super(date, amount, description);
		this.sender = sender;
		this.recipient = recipient;
	}
	
	/**
	 * Copy-Konstruktor
	 * 
	 * @param t
	 */
	public Transfer(Transfer t) {
		this.date = t.date;
		this.amount = t.amount;
		this.description = t.description;
		this.sender = t.sender;
		this.recipient = t.recipient;
	}
	
	/**
	 * Override von Object.toString()
	 * 
	 * @return Transfer-Object als String
	 */
	@Override
	public String toString() {
		return super.toString() + ", Sender: " + sender + ", Recipient: " + recipient;
	}
	
	/**
	 * Berechnet die Kosten einer �berweisung
	 * 
	 * @return amount
	 */
	public double calculate() {
		return amount;
	}
	
	/**
	 * Override von Object.equals(Object o)
	 * 
	 * @return true or false
	 */
	@Override
	public boolean equals(Object o) {
		if (o instanceof Transfer) {
			if (super.equals(o) && sender == ((Transfer) o).getSender() && recipient == ((Transfer) o).getRecipient()) {
				return true;
			}
		}
		return false;
	}
}