/**
 * Ein Bank-Programm zum Praktikum OOS WS 2021/22
 * Praktikum 2
 *
 * @author Marius Frohnhofen
 */

package bank;

/**
 * Abstrakte Klasse Transaction
 * 
 * Von dieser Klasse erben Payment und Transfer
 */
public abstract class Transaction implements CalculateBill {
	
	/**
	 * Zeitpunkt einer Transaktion
	 */
	protected String date;
	
	/**
	 * Geldmenge einer Transaktion
	 */
	protected double amount;
	
	/**
	 * Beschreibung einer Transaktion
	 */
	protected String description;
	
	/**
	 * Get-Methode f�r Date
	 * 
	 * @return date-Attribut
	 */
	public String getDate() {
		return date;
	}
	
	/**
	 * Set-Methode f�r Date
	 * 
	 * @param date
	 */
	public void setDate(String date) {
		this.date = date;
	}
	
	/**
	 * Get-Methode f�r Amount
	 * 
	 * @return amount-Attribut
	 */
	public double getAmount() {
		return amount;
	}
	
	/**
	 * Set-Methode f�r Amount
	 * 
	 * @param amount
	 */
	public void setAmount(double amount) {
		this.amount = amount;
	}
	
	/**
	 * Get-Methode f�r Description
	 * 
	 * @return description-Attribut
	 */
	public String getDescription() {
		return description;
	}
	
	/**
	 * Set-Methode f�r Description
	 * 
	 * @param description
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	
	/**
	 * Leerer Konstruktor
	 */
	public Transaction() {
		
	}
	
	/**
	 * Konstruktor, welcher date, amount und description setzt
	 * 
	 * @param date
	 * @param amount
	 * @param description
	 */
	public Transaction(String date, double amount, String description) {
		this.date = date;
		this.amount = amount;
		this.description = description;
	}
	
	/**
	 * Override von Object.toString()
	 * 
	 * @return Transaction-Object als String
	 */
	@Override
	public String toString() {
		return "Date: " + date + ", Amount: " + calculate() + ", Description: " + description;
	}
	
	/**
	 * Override von Object.equals(Object o)
	 * 
	 * @return true or false
	 */
	@Override
	public boolean equals(Object o) {
		if (date == ((Transaction) o).getDate() && amount == ((Transaction) o).getAmount() && description == ((Transaction) o).getDescription()) {
			return true;
		}
		return false;
	}
}
