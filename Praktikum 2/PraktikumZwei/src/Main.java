/**
 * Ein Bank-Programm zum Praktikum OOS WS 2021/22
 * Praktikum 2
 *
 * @author Marius Frohnhofen
 */

import bank.*;

/**
 * Klasse Main, hier werden Tests zur Implementation durchgeführt
 */
public class Main {
	public static void main(String[] args) {
		//Allgemeiner Test und toString-Methode
		
		//Payment-Objekt mit Setter-Methoden
		Payment p = new Payment("", 0, "");
		p.setDate("Test Date");
		p.setAmount(25);
		p.setDescription("Test Beschreibung");
		p.setIncomingInterest(0.1);
		p.setOutgoingInterest(0.2);
		
		//Ausgabe mit Getter-Methoden
		System.out.println("p:");
		System.out.println();
		System.out.println("Date: " + p.getDate());
		System.out.println("Amount: " + p.getAmount());
		System.out.println("Description: " + p.getDescription());
		System.out.println("Incoming Interest: " + p.getIncomingInterest());
		System.out.println("Outgoing Interest: " + p.getOutgoingInterest());
		System.out.println();
		
		//Transfer-Objekt mit Setter-Methoden
		Transfer t = new Transfer("", 0, "");
		t.setDate("Test Date");
		t.setAmount(25);
		t.setDescription("Test Beschreibung");
		t.setSender("Test Sender");
		t.setRecipient("Test Recipient");
		
		//Ausgabe mit Getter-Methoden
		System.out.println("t:");
		System.out.println();
		System.out.println("Date: " + t.getDate());
		System.out.println("Amount: " + t.getAmount());
		System.out.println("Description: " + t.getDescription());
		System.out.println("Sender: " + t.getSender());
		System.out.println("Recipient: " + t.getRecipient());
		System.out.println();
		
		//Payment-Objekt mit Konstruktor Payment(String date, double amount, String description)
		Payment pEins = new Payment("Test Date 1", 10, "Test Beschreibung 1");
		
		//Payment-Objekt mit Konstruktor Payment(String date, double amount, String description, double incomingInterest, double outgoingInterest)
		Payment pZwei = new Payment("Test Date 2", 20, "Test Beschreibung 2", 0.4, 0.5);
		
		//Payment-Objekt mit Copy-Konstruktor
		Payment pDrei = new Payment(pZwei);
		
		//Transfer-Objekt mit Konstruktor Transfer(String date, double amount, String description)
		Transfer tEins = new Transfer("Test Date 1", 10, "Test Beschreibung 1");
		
		//Transfer-Objekt mit Konstruktor Transfer(String date, double amount, String description, String sender, String recipient)
		Transfer tZwei = new Transfer("Test Date 2", 20, "Test Beschreibung 2", "Test Sender 1", "Test Recipient 1");
		
		//Transfer-Objekt mit Copy-Konstruktor
		Transfer tDrei = new Transfer(tZwei);
		
		//Ausgabe der einzelnen Objekte
		System.out.println("pEins:");
		System.out.println();
		System.out.println(pEins.toString());
		
		System.out.println();
		System.out.println("pZwei:");
		System.out.println();
		System.out.println(pZwei.toString());
		
		System.out.println();
		System.out.println("pDrei:");
		System.out.println();
		System.out.println(pDrei.toString());
		
		System.out.println();
		System.out.println("tEins:");
		System.out.println();
		System.out.println(tEins.toString());
		
		System.out.println();
		System.out.println("tZwei:");
		System.out.println();
		System.out.println(tZwei.toString());
		
		System.out.println();
		System.out.println("tDrei:");
		System.out.println();
		System.out.println(tDrei.toString());

		
		
		
		//Test der calculate-Methode
		
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		

		Payment einzahlung = new Payment("Datum der Einzahlung", 1000, "Einzahlungs-Beschreibung", 0.05, 0.1);
		Payment auszahlung = new Payment("Datum der Auszahlung", -1000, "Auszahlungs-Beschreibung", 0.05, 0.1);
		
		Transfer ueberweisung = new Transfer("Datum der Überweisung", 1000, "Überweisungs-Beschreibung", "Sender", "Recipient");
		
		
		System.out.println(einzahlung.toString());
		System.out.println(auszahlung.toString());
		System.out.println(ueberweisung.toString());
		
		System.out.println();
		
		System.out.println("Calculate Einzahlung: " + einzahlung.calculate());
		System.out.println("Calculate Auszahlung: " + auszahlung.calculate());
		System.out.println("Calculate Überweisung: " + ueberweisung.calculate());
		
		
	}
}