package bank;

//Diese Klasse soll im Kontext von �berweisungen verwendet werden
public class Transfer {
	//Zeitpunkt einer Ein- oder Auszahlung
	private String date;
	//Geldmenge einer Überweisung
	private double amount;
	//Zusätzliche Beschreibung des Vorgangs
	private String description;
	
	//Gibt an, welcher Akteur die Geldmenge, die in amount angegeben wurde, überwiesen hat
	private String sender;
	//Gibt an, welcher Akteur die Geldmenge, die in amount angegeben wurde, überwiesen bekommen hat
	private String recipient;
	
	//Gibt date-Attribut zurück
	public String getDate() {
		return date;
	}
	
	//Setzt date-Attribut
	public void setDate(String date) {
		this.date = date;
	}
	
	//Gibt amount-Attribut zurück
	public double getAmount() {
		return amount;
	}
	
	//Setzt amount-Attribut zurück
	public void setAmount(double amount) {
		this.amount = amount;
	}
	
	//Gibt description-Attribut zurück
	public String getDescription() {
		return description;
	}
	
	//Setzt description-Attribut
	public void setDescription(String description) {
		this.description = description;
	}
	
	//Gibt sender-Attribut zurück
	public String getSender() {
		return sender;
	}
	
	//Setzt sender-Attribut
	public void setSender(String sender) {
		this.sender = sender;
	}
	
	//Gibt recipient-Attribut zurück
	public String getRecipient() {
		return recipient;
	}
	
	//Setzt recipient-Attribut
	public void setRecipient(String recipient) {
		this.recipient = recipient;
	}
	
	//Konstruktor, mit welchem date, amount und description gesetzt werden
	public Transfer(String date, double amount, String description) {
		this.date = date;
		this.amount = amount;
		this.description = description;
	}
	
	//Konstruktor, mit welchem date, amount, description, sender und recipient gesetzt werden
	public Transfer(String date, double amount, String description, String sender, String recipient) {
		this(date, amount, description);
		this.sender = sender;
		this.recipient = recipient;
	}
	
	//Copy-Konstruktor
	public Transfer(Transfer t) {
		this.date = t.date;
		this.amount = t.amount;
		this.description = t.description;
		this.sender = t.sender;
		this.recipient = t.recipient;
	}
	
	//Gibt alle Attribute des Objekts in der Konsole aus
	public void printObject() {
		System.out.println("Date: " + date);
		System.out.println("Amount: " + amount);
		System.out.println("Description: " + description);
		System.out.println("Sender: " + sender);
		System.out.println("Recipient: " + recipient);
	}
}