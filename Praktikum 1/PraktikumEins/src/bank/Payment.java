package bank;

//Diese Klasse soll Ein- und Auszahlungen repr�sentieren
public class Payment {
	//Zeitpunkt einer Ein- oder Auszahlung
	private String date;
	//Geldmenge einer Überweisung
	private double amount;
	//Zusätzliche Beschreibung des Vorgangs
	private String description;
	
	//Zinsen (0 bis 1), die bei einer Einzahlung anfallen
	private double incomingInterest;
	//Zinsen (0 bis 1), die bei einer Auszahlung anfallen
	private double outgoingInterest;
	
	//Gibt date-Attribut zurück
	public String getDate() {
		return date;
	}
	
	//Setzt date-Attribut
	public void setDate(String date) {
		this.date = date;
	}
	
	//Gibt amount-Attribut zurück
	public double getAmount() {
		return amount;
	}
	
	//Setzt amount-Attribut
	public void setAmount(double amount) {
		this.amount = amount;
	}
	
	//Gibt description-Attribut zurück
	public String getDescription() {
		return description;
	}
	
	//Setzt description-Attribut
	public void setDescription(String description) {
		this.description = description;
	}
	
	//Gibt incomingInterest-Attribut zurück
	public double getIncomingInterest() {
		return incomingInterest;
	}
	
	//Setzt incomingInterest-Attribut
	public void setIncomingInterest(double incomingInterest) {
		this.incomingInterest = incomingInterest;
	}
	
	//Gibt outgoingInterest-Attribut zurück
	public double getOutgoingInterest() {
		return outgoingInterest;
	}
	
	//Setzt outgoingInterest-Attribut
	public void setOutgoingInterest(double outgoingInterest) {
		this.outgoingInterest = outgoingInterest;
	}
	
	//Konstruktor, mit welchem date, amount und description gesetzt werden
	public Payment(String date, double amount, String description) {
		this.date = date;
		this.amount = amount;
		this.description = description;
	}
	
	//Konstruktor, mit welchem date, amount, description, incomingInterest und outgoingInterest gesetzt werden
	public Payment(String date, double amount, String description, double incomingInterest, double outgoingInterest) {
		this(date, amount, description);
		this.incomingInterest = incomingInterest;
		this.outgoingInterest = outgoingInterest;
	}
	
	//Copy-Konstruktor
	public Payment(Payment p) {
		this.date = p.date;
		this.amount = p.amount;
		this.description = p.description;
		this.incomingInterest = p.incomingInterest;
		this.outgoingInterest = p.outgoingInterest;
	}
	
	//Gibt alle Attribute des Objekts in der Konsole aus
	public void printObject() {
		System.out.println("Date: " + date);
		System.out.println("Amount: " + amount);
		System.out.println("Description: " + description);
		System.out.println("Incoming Interest: " + incomingInterest);
		System.out.println("Outgoing Interest: " + outgoingInterest);
	}
}